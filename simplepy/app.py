import time
import os

duration = 5

# si la variable d'envir est trouvé on ecrase la 
if "SLEEP_DURATION" in os.environ:
    duration = int(os.environ["SLEEP_DURATION"])

while True:
    print("Simple app python infinite loop")
    time.sleep(duration)